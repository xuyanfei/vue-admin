//引入axios设置基础路径
import axios from "axios";
import { MessageBox } from 'element-ui'

// axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
axios.defaults.baseURL = "http://timemeetyou.com:8889/api/private/v1/";
console.log(process,'--->process');
console.log(process.env,'--->process.env');
//挂载请求拦截器 (相当于请求的预验证，请求到达服务器之前先验证这次请求)
axios.interceptors.request.use(config => {
  //为请求头添加对象，添加token验证的Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem("token");
  // console.log(config);
  return config;
});
// response 拦截器
axios.interceptors.response.use(
  response => {
    const code = response.status;
    if (code == 504) {
      MessageBox.confirm("网络请求超时", "提示", {
        showCancelButton: false,
        type: "warning"
      });
      return Promise.reject(error);
    }
    if (code < 200 || code > 300) {
      MessageBox.confirm(response.message, "提示", {
        showCancelButton: false,
        type: "warning"
      });
      return Promise.reject("error");
    } else {
      // console.log(response.data, "拦截 请求的 response.data");
      // 后台报错 导致前端 页面卡顿崩溃
      if (response.data.status === -1) {
        let statusText = "";
        if (response.data.statusText) {
          if (response.data.statusText.indexOf(":") !== -1) {
            statusText = response.data.statusText.replace(":", "");
          }
        }
        if (statusText) {
          console.log(statusText, "44444");
          MessageBox.confirm(statusText, "提示", {
            showCancelButton: false,
            type: "warning"
          });
        } else {
          MessageBox.confirm(response.data.statusText, "提示", {
            showCancelButton: false,
            type: "warning"
          });
        }
        return Promise.reject(response.data);
      }
      return response;
    }
  },
  error => {
    let code = 0;
    try {
      code = error.response.status;
    } catch (e) {
      // console.log(error,'--->error');
      // console.log(typeof error,'--->typeof error'); // object
      // console.log(code,'code');
      // console.log(e,'--->e');
      if (error.toString().indexOf("Error: timeout") !== -1) {
        MessageBox.confirm("网络请求超时", "提示", {
          showCancelButton: false,
          type: "warning"
        });
        return Promise.reject(error);
      }
      if (error.toString().indexOf("Error: Network Error") !== -1) {
        MessageBox.confirm("网络请求错误", "提示", {
          showCancelButton: false,
          type: "warning"
        });
        return Promise.reject(error);
      }
    }
    if (code === 401) {
      MessageBox.confirm("登录状态已过期，请重新登录", "系统提示", {
        confirmButtonText: "重新登录",
        showCancelButton: false,
        showClose: false,
        type: "warning"
      }).then(() => {
        // store.dispatch("LogOut").then(() => {
        //   location.reload(); // 为了重新实例化vue-router对象 避免bug
        // });
      });
    } else if (code === 403) {
      router.push({ path: "/401" });
    } else {
      const errorMsg = error.response.data.message;
      if (errorMsg !== undefined) {
        MessageBox.confirm(errorMsg, "提示", {
          confirmButtonText: "确定",
          showCancelButton: false,
          showClose: false,
          type: "warning"
        });
      }
    }
    return Promise.reject(error);
  }
);
export default axios;
