import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Tologin from '../components/Tologin.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import UserList from '../components/users/UserList.vue'
import Rights from '../components/power/Rights.vue'
import Roles from '../components/power/Roles.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/login',
    component: Login
  },
  {
    path: '/',
    redirect: '/tologin'
  },
  {
    path: '/tologin',
    component: Tologin
  },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [{
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/users',
        component: UserList
      },
      {
        path: '/rights',
        component: Rights
      },
      {
        path: '/roles',
        component: Roles
      }

    ]
  }
]

const router = new VueRouter({
  routes
})

//挂载路由导航守卫
router.beforeEach((to, form, next) => {
  //to表示将要跳转到的页面
  //from表示从那个页面跳转来的
  // next表示放行函数 next() 直接放行 next('/login') 强制跳转
  if (to.path === '/tologin') return next();
  const tokenStr = window.sessionStorage.getItem('token');
  if (!tokenStr) return next('/tologin');
  next();
})


export default router
