import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./plugins/element.js";
// 引入全局样式表
import "./assets/css/global.css";
// 引入iconfont图标样式
import "./assets/font/iconfont.css";
// 引入具有拦截请求的axios
import xhrAxios from "./utils/xhrAxios";
Vue.prototype.$http = xhrAxios;

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
